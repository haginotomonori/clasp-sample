[公式document](https://github.com/google/clasp)

## docker-composer

```bash
docker-compoer up -d --build
docker exec -it clasp "/bin/bash"
```

## 作業の流れ
### login
```bash
# loginする
clasp login --no-localhost
```

### 新規作成

```bash
mkdir myFirstApp; cd $_
clasp create myFirstApp --rootDir ./src
clasp pull
# myFirstApp/src/myFirstApp.ts を作って編集
clasp push
# google scriptを開く
clasp open
```

### gasで作成済みを編集

```bash
mkdir mySecondApp; cd $_
clasp clone <scriptURL> --rootDir ./src
# mySecondApp/src/xxx を編集
clasp push
# google scriptを開く
clasp open
```

### memo

* .ts をpushするとトランスパイルしてくれる
* .js　も使えるがES6非対応なので頑張る
* clasp create でスプレッドシートに紐づいたスクリプトも作れる
* clasp push --watch で保存したら勝手にpushしてくれる