FROM node:10.15.0-slim

ENV NODE_ENV=local

WORKDIR /src

RUN npm i -g npm && \
    npm i -g @google/clasp @types/google-apps-script && \
    apt-get -y update && apt-get -y upgrade && apt-get -y install zsh vim
